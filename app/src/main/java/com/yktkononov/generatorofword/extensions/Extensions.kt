package com.yktkononov.generatorofword.extensions

import android.view.View

private var lastClickTimestamp = 0L

fun View.setThrottledClickListener(delay: Long = 1000L, clickListener: (View) -> Unit) {
    setOnClickListener {
        val currentTimestamp = System.currentTimeMillis()
        val delta = currentTimestamp - lastClickTimestamp
        //0L for hidden bug: launch app -> move system time back -> return to app ...
        if (delta !in 0L..delay) {
            lastClickTimestamp = currentTimestamp
            clickListener.invoke(this)
        }
    }
}