package com.yktkononov.generatorofword.wikiHelper

import android.util.Log
import com.yktkononov.generatorofword.Language
import com.yktkononov.generatorofword.entity.WikiData
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.IOException

object WiktionaryApi {

    private const val WIKTIONARY_URL = "https://%s.wiktionary.org/wiki/%s"

    fun getWiki(word: String, lang: Language = Language.ENGLISH): WikiData {
        try {
            val requestUrl = WIKTIONARY_URL.format(lang.ISOCode, word.toLowerCase())
            val document: Document = Jsoup.connect(requestUrl).get()
            val title: String? = document.select("p").select(if (lang == Language.ENGLISH) "strong" else "b").first()?.text()
            val details: List<String>? =
                document.select("ol")
                    .first()?.select("li")?.map { it.text() }?.filter { !it.isNullOrBlank() }
            return WikiData(title ?: word, details)
        } catch (ex: IOException) {
            Log.e("wikiHelper", "ERROR", ex)
        }
        return WikiData(word)
    }
}