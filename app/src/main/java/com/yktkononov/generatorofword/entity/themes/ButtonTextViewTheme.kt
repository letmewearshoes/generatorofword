package com.yktkononov.generatorofword.entity.themes

data class ButtonTextViewTheme(
    val textColorResId: Int,
    val backgroundResId: Int
)