package com.yktkononov.generatorofword.entity.themes

data class StandardTextViewTheme(
    val textColorResId: Int
)