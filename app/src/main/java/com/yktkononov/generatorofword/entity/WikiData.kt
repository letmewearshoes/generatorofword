package com.yktkononov.generatorofword.entity

data class WikiData(
    val title: String,
    val descriptions: List<String>? = null
)