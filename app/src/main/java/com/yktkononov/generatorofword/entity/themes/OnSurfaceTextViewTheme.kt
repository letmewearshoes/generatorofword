package com.yktkononov.generatorofword.entity.themes

data class OnSurfaceTextViewTheme(
    val textColorResId: Int
)