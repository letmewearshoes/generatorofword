package com.yktkononov.generatorofword.entity.themes

data class CardViewTheme(
    val backgroundResId: Int
)