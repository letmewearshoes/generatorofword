package com.yktkononov.generatorofword

enum class WordsLanguage constructor(val ISOCode: String) {
    ENGLISH("en"),
    RUSSIAN("ru"),
}