package com.yktkononov.generatorofword.model

import com.yktkononov.generatorofword.BuildConfig
import com.yktkononov.generatorofword.Language
import java.util.*

object LanguageHolder {
    private var language: Language? = null

    fun init(defaultLanguage: Language, debugLanguage: Language? = null): Language {
        if (BuildConfig.DEBUG && debugLanguage != null) {
            language = debugLanguage
        } else {
            language = Language.values().find { it.ISOCode == Locale.getDefault().language } ?: defaultLanguage
        }
        return language!!
    }

    fun get(): Language = language ?: init(Language.ENGLISH)
}