package com.yktkononov.generatorofword.model

import android.content.Context
import io.reactivex.subjects.BehaviorSubject
import java.lang.Exception
import java.util.*

object FavoriteWordsHolder {
    private const val FAVORITES_KEY = "com.yktkononov.generatorofnoun.LIST"
    private val favorites: TreeSet<String> = TreeSet()
    private var changed = false
    private val state: BehaviorSubject<Collection<String>> = BehaviorSubject.createDefault(favorites)

    fun load(context: Context?) {
        val sp = context?.getSharedPreferences(FAVORITES_KEY, Context.MODE_PRIVATE)
        favorites.clear()
        favorites.addAll(sp?.getStringSet(FAVORITES_KEY, emptySet()) ?: emptySet())
    }

    fun save(context: Context?) {
        if (changed && context != null) {
            val sp = context.getSharedPreferences(FAVORITES_KEY, Context.MODE_PRIVATE)
            val editor = sp.edit()
            editor.remove(FAVORITES_KEY)
                .commit()
            editor.putStringSet(FAVORITES_KEY, favorites)
                .commit()
            changed = false
        }
    }

    fun addOrRemove(word: String): Boolean { //true - added, false - removed
        changed = true
        if (add(word)) return true
        else if (remove(word)) return false
        else throw Exception("FavoriteWordsHolder: Не удалось выполнить операцию")
    }

    fun contains(word: String): Boolean = favorites.contains(word)

    fun add(word: String): Boolean = favorites.add(word).also { if (it) state.onNext(favorites) }

    fun remove(word:String): Boolean = favorites.remove(word).also { if (it) state.onNext(favorites) }

    fun get(): List<String> = favorites.toList()

    fun listen(): BehaviorSubject<Collection<String>> = state
}