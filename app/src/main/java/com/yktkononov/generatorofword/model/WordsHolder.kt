package com.yktkononov.generatorofword.model

import android.content.Context
import com.yktkononov.generatorofword.Language
import com.yktkononov.generatorofword.R
import com.yktkononov.generatorofword.WordsLanguage
import java.io.BufferedReader
import java.io.InputStreamReader

object WordsHolder {
    private const val WORDS_LANGUAGE_KEY = "WORDS_LANGUAGE_KEY"
    private val words: MutableList<String> = mutableListOf()
    var currentLanguage: WordsLanguage? = null
        private set

    fun init(context: Context?, systemLanguage: Language) {
        val _currentLanguage: WordsLanguage?
        val sp = context?.getSharedPreferences(WORDS_LANGUAGE_KEY, Context.MODE_PRIVATE)
        val languageISO = sp?.getString(WORDS_LANGUAGE_KEY, null)
        if (languageISO.isNullOrBlank()) {
            when (systemLanguage) {
                Language.RUSSIAN, Language.ARMENIAN, Language.BELARUSIAN,
                Language.AZERBAIJANI, Language.UKRAINIAN -> _currentLanguage = WordsLanguage.RUSSIAN
                else -> _currentLanguage = WordsLanguage.ENGLISH
            }
        } else {
            _currentLanguage = WordsLanguage.values().find { it.ISOCode == languageISO } ?: WordsLanguage.ENGLISH
        }

        if (currentLanguage != _currentLanguage && context != null){
            currentLanguage = _currentLanguage
            loadData(context, _currentLanguage)
        }
    }

    fun changeWordsLanguage(context: Context?, language: WordsLanguage) {
        if (currentLanguage != language && context != null){
            currentLanguage = language
            val sp = context.getSharedPreferences(WORDS_LANGUAGE_KEY, Context.MODE_PRIVATE)
            val editor = sp.edit()
            editor.putString(WORDS_LANGUAGE_KEY, language.ISOCode)
                .commit()
            loadData(context, language)
        }
    }

    private fun loadData(context: Context, language: WordsLanguage) {
        words.clear()
        when (language) {
            WordsLanguage.ENGLISH -> words.addAll(BufferedReader(InputStreamReader(context.resources.openRawResource(R.raw.words_en))).readLines())
            WordsLanguage.RUSSIAN -> words.addAll(BufferedReader(InputStreamReader(context.resources.openRawResource(R.raw.words_ru), "Windows-1251")).readLines())
        }
    }

    fun get(): List<String> = words

    fun getRandom(): String = words.random()
}