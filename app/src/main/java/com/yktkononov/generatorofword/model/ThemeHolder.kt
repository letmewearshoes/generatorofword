package com.yktkononov.generatorofword.model

import android.content.Context
import com.yktkononov.generatorofword.Theme
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

object ThemeHolder {

    private const val THEME_KEY = "THEME_KEY"
    private lateinit var theme: Theme
    private val state: BehaviorSubject<Theme> = BehaviorSubject.create()

    fun init(context: Context?) {
        val sp = context?.getSharedPreferences(THEME_KEY, Context.MODE_PRIVATE)
        val themeString = sp?.getString(THEME_KEY, null)
        when (themeString) {
            Theme.DAY.name -> theme = Theme.DAY
            Theme.NIGHT.name -> theme = Theme.NIGHT
            else -> theme = Theme.DAY
        }
        state.onNext(theme)
    }

    fun get(): Theme = theme

    fun set(theme: Theme) {
        if (this.theme != theme) {
            this.theme = theme
            state.onNext(theme)
        }
    }

    fun save(context: Context?) {
        if (context != null) {
            val sp = context.getSharedPreferences(THEME_KEY, Context.MODE_PRIVATE)
            val editor = sp.edit()
            editor.putString(THEME_KEY, theme.name)
                .commit()
        }
    }

    fun listen(): Observable<Theme> = state
}