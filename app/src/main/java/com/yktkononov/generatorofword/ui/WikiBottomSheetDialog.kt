package com.yktkononov.generatorofword.ui

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yktkononov.generatorofword.Language
import com.yktkononov.generatorofword.R
import com.yktkononov.generatorofword.ui.views.BaseBottomSheetDialog
import com.yktkononov.generatorofword.wikiHelper.WiktionaryApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_fragment_wiki.*

class WikiBottomSheetDialog : BaseBottomSheetDialog() {

    companion object {
        private const val TITLE_TAG = "TITLE_TAG"
        private const val LANG_TAG = "LANG_TAG"
        fun newInstance(title: String, lang: Language) = WikiBottomSheetDialog().apply {
            arguments = Bundle().apply {
                putString(TITLE_TAG, title)
                putSerializable(LANG_TAG, lang)
            }
        }
    }

    override val backgroundLayout: ViewGroup
        get() = wiki_layout

    private val title: String get() = arguments?.getString(TITLE_TAG) ?: ""
    private val lang: Language get() = arguments?.getSerializable(LANG_TAG) as Language? ?: Language.ENGLISH

    private val adapter: WikiDetailsAdapter by lazy { WikiDetailsAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.dialog_fragment_wiki, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        wiki_textview.text = title
        with(wiki_recyclerview) {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = this@WikiBottomSheetDialog.adapter
            val density = resources.displayMetrics.density
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    super.getItemOffsets(outRect, view, parent, state)

                    if (parent.indexOfChild(view) == 0) {
                        outRect.top = (10 * density).toInt()
                    }
                    outRect.left = (10 * density).toInt()
                    outRect.right = (10 * density).toInt()
                    outRect.bottom = (10 * density).toInt()
                }
            })
        }

        Single.fromCallable { WiktionaryApi.getWiki(title, lang) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { wiki_progressbar?.visibility = View.VISIBLE }
            .doFinally { wiki_progressbar?.visibility = View.GONE }
            .subscribe(
                {
                    wiki_textview?.text = it.title
                    adapter.setData(it.descriptions ?: emptyList())
                },
                {
                    Log.e("error", "Error while loading", it)
                }
            )
            .connect()
    }

    private class WikiDetailsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private val list: MutableList<String> = mutableListOf()

        fun setData(data: List<String>) {
            list.clear()
            list.addAll(data)
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_wiki_details, parent, false))

        override fun getItemCount(): Int = list.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
            (holder as ViewHolder).bind(list[position])

        private class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            private val textView: TextView = view.findViewById(R.id.wiki_details_textview)

            fun bind(data: String) {
                textView.text = data
            }
        }
    }
}