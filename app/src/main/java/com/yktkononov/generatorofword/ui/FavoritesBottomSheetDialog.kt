package com.yktkononov.generatorofword.ui

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yktkononov.generatorofword.R
import com.yktkononov.generatorofword.model.FavoriteWordsHolder
import com.yktkononov.generatorofword.model.LanguageHolder
import com.yktkononov.generatorofword.ui.views.BaseBottomSheetDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_fragment_favorites.*

class FavoritesBottomSheetDialog : BaseBottomSheetDialog(), FavoritesAdapter.Listener {

    override val backgroundLayout: ViewGroup
        get() = favorites_layout
    private val adapter: FavoritesAdapter by lazy { FavoritesAdapter(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.dialog_fragment_favorites, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(favorites_recyclerview) {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = this@FavoritesBottomSheetDialog.adapter
            val density = resources.displayMetrics.density
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    super.getItemOffsets(outRect, view, parent, state)

                    if (parent.indexOfChild(view) == 0) {
                        outRect.top = (10 * density).toInt()
                    }
                    outRect.left = (10 * density).toInt()
                    outRect.right = (10 * density).toInt()
                    outRect.bottom = (10 * density).toInt()
                }
            })
        }

        FavoriteWordsHolder.listen()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    adapter.setData(it)
                },
                {
                    Log.e("error", "Error while loading", it)
                }
            )
            .connect()
    }

    override fun onWordClicked(word: String) {
        WikiBottomSheetDialog.newInstance(word, LanguageHolder.get())
            .show(childFragmentManager, null)
    }

    override fun onDeleteClicked(word: String) {
        FavoriteWordsHolder.remove(word)
    }
}

class FavoritesAdapter(private val listener: Listener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val list: MutableList<String> = mutableListOf()

    fun setData(data: Collection<String>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_favorite, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        (holder as ViewHolder).bind(list[position])

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val textView: TextView = view.findViewById(R.id.favorite_textview_word)
        private val deleteImageView: ImageView = view.findViewById(R.id.favorite_imageview_delete)

        init {
            view.setOnClickListener { listener.onWordClicked(textView.text.toString()) }
            deleteImageView.setOnClickListener { listener.onDeleteClicked(textView.text.toString()) }
        }

        fun bind(data: String) {
            textView.text = data
        }
    }

    interface Listener {
        fun onWordClicked(word: String)
        fun onDeleteClicked(word: String)
    }
}