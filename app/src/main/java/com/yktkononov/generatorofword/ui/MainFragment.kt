package com.yktkononov.generatorofword.ui

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageSwitcher
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import com.yktkononov.generatorofword.R
import com.yktkononov.generatorofword.Theme
import com.yktkononov.generatorofword.WordsLanguage
import com.yktkononov.generatorofword.extensions.setThrottledClickListener
import com.yktkononov.generatorofword.model.FavoriteWordsHolder
import com.yktkononov.generatorofword.model.LanguageHolder
import com.yktkononov.generatorofword.model.ThemeHolder
import com.yktkononov.generatorofword.model.WordsHolder
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.fragment_main.*
import java.util.concurrent.TimeUnit

class MainFragment : Fragment() {

    private val layoutResId: Int = R.layout.fragment_main

    private val favorites: FavoriteWordsHolder by lazy { FavoriteWordsHolder }
    private var disposables: CompositeDisposable? = null
    private var currentWord: BehaviorSubject<String> = BehaviorSubject.createDefault("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        disposables = CompositeDisposable()
        return inflater.inflate(layoutResId, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        main_language_button.displayedChild = if (WordsHolder.currentLanguage == WordsLanguage.RUSSIAN) 0 else 1

        main_language_button.setOnClickListener { imageSwitcher ->
            Observable.fromCallable {
                imageSwitcher as ImageSwitcher
                WordsHolder.changeWordsLanguage(
                    context,
                    if (imageSwitcher.displayedChild == 0) WordsLanguage.ENGLISH else WordsLanguage.RUSSIAN
                )
            }
                .delay(1000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { main_progress_layout?.visibility = View.VISIBLE }
                .doFinally { main_progress_layout?.visibility = View.GONE }
                .subscribe {
                    main_language_button?.apply {
                        displayedChild = if (WordsHolder.currentLanguage == WordsLanguage.RUSSIAN) 0 else 1
                    }
                    currentWord.onNext("")
                }
        }

        main_fav_button.setOnClickListener {
            val word = main_title_textview.text.toString()
            if (word.isNotBlank()) {
                FavoriteWordsHolder.addOrRemove(word)
            }
        }

        main_list_button.setThrottledClickListener {
            FavoritesBottomSheetDialog()
                .show(childFragmentManager, null)
        }

        main_generate_button.setOnClickListener {
            currentWord.onNext(WordsHolder.getRandom())
        }

        main_info_button.setThrottledClickListener {
            val word = main_title_textview.text.toString()
            if (word.isNotBlank()) {
                WikiBottomSheetDialog.newInstance(word, LanguageHolder.get())
                    .show(childFragmentManager, null)
            } else {
                //todo: show about app dialog
            }
        }

        main_title_textview.setOnLongClickListener {
            val textToCopy = (it as TextView).text
            if (!textToCopy.isNullOrBlank()) {
                val clipboardManager = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                if (clipboardManager != null) {
                    clipboardManager.setPrimaryClip(ClipData.newPlainText("", textToCopy))
                    Toast.makeText(context, "copied", Toast.LENGTH_SHORT).show()
                }
            }
            return@setOnLongClickListener true
        }

        main_title_textview.addTextChangedListener {
            val word = it.toString()
            if (!word.isNullOrBlank()) {
                main_fav_button.displayedChild = if (FavoriteWordsHolder.contains(word)) 1 else 0
                main_fav_button.visibility = View.VISIBLE
            } else {
                main_fav_button.visibility = View.GONE
            }
        }

        main_daynight_swithcer.setOnClickListener { (activity as? MainActivity)?.changeTheme(it as ImageSwitcher) }

        ThemeHolder.listen()
            .subscribe { main_daynight_swithcer.displayedChild = if (it == Theme.DAY) 0 else 1 }
            .connect()

        FavoriteWordsHolder.listen()
            .subscribe { main_fav_button.displayedChild = if (it.contains(currentWord.value)) 1 else 0 }
            .connect()

        currentWord.subscribe { main_title_textview.text = it }.connect()
    }

    override fun onResume() {
        super.onResume()
        favorites.load(context)
    }

    override fun onPause() {
        super.onPause()
        favorites.save(context)
    }

    override fun onDestroyView() {
        disposables?.dispose()
        disposables = null
        super.onDestroyView()
    }

    private fun Disposable.connect() = disposables?.add(this)
}