package com.yktkononov.generatorofword.ui.views

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.yktkononov.generatorofword.R
import com.yktkononov.generatorofword.Theme
import com.yktkononov.generatorofword.model.ThemeHolder
import io.reactivex.disposables.Disposable

class DayNightImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ImageView(context, attrs, defStyleAttr) {

    private var disposable: Disposable? = null
    private val backgroundResIdDay: Int
    private val backgroundResIdNight: Int

    init {
        context.theme.obtainStyledAttributes(attrs, R.styleable.DayNightImageView, 0, 0).apply {
            try {
                backgroundResIdDay = getResourceId(R.styleable.DayNightImageView_srcDay, 0)
                backgroundResIdNight = getResourceId(R.styleable.DayNightImageView_srcNight, 0)
            } finally {
                recycle()
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        disposable = ThemeHolder
            .listen()
            .subscribe { setTheme(it) }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        disposable?.dispose()
    }

    private fun setTheme(theme: Theme) {
        if (theme == Theme.NIGHT) setBackgroundResource(backgroundResIdNight)
        else setBackgroundResource(backgroundResIdDay)
    }
}