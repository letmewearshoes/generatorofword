package com.yktkononov.generatorofword.ui

import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.ImageSwitcher
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.yktkononov.generatorofword.R
import com.yktkononov.generatorofword.Theme
import com.yktkononov.generatorofword.model.ThemeHolder
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.hypot

class MainActivity : AppCompatActivity() {

    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.apply {
                decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                statusBarColor = ContextCompat.getColor(context, R.color.colorTransparent80)
                navigationBarColor = ContextCompat.getColor(context, R.color.colorTransparent)
            }
        }

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.main_container, MainFragment())
                .commit()
        }

        disposable = ThemeHolder
            .listen()
            .subscribe {
                if (it == Theme.DAY) main_container.setBackgroundResource(R.color.colorBackground)
                else if (it == Theme.NIGHT) main_container.setBackgroundResource(R.color.colorBackground1)
            }
    }

    fun changeTheme(dayNightSwither: ImageSwitcher) {
//        if (main_fullscreen_imageview.isVisible) {
//            return
//        }
//
//        val w = main_container.measuredWidth
//        val h = main_container.measuredHeight
//
//        val bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
//        val canvas = Canvas(bitmap)
//        main_container.draw(canvas)
//
//        main_fullscreen_imageview.setImageBitmap(bitmap)
//        main_fullscreen_imageview.isVisible = true
//
//        val finalRadius = hypot(w.toFloat(), h.toFloat())

        when (dayNightSwither.displayedChild) {
            0 -> { //day
                ThemeHolder.set(Theme.NIGHT)
            }
            1 -> { //night
                ThemeHolder.set(Theme.DAY)
            }
            else -> {}
        }

//        val anim = ViewAnimationUtils.createCircularReveal(main_container, w / 2, h / 2, 0f, finalRadius)
//        anim.duration = 400L
//        anim.doOnEnd {
//            main_fullscreen_imageview.setImageDrawable(null)
//            main_fullscreen_imageview.isVisible = false
//        }
//        anim.start()
    }

    override fun onPause() {
        super.onPause()
        ThemeHolder.save(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }
}
