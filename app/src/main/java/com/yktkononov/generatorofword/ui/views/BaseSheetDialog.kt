package com.yktkononov.generatorofword.ui.views

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.yktkononov.generatorofword.R
import com.yktkononov.generatorofword.Theme
import com.yktkononov.generatorofword.model.ThemeHolder
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseBottomSheetDialog : BottomSheetDialogFragment() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    abstract val backgroundLayout: ViewGroup

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireContext(), theme).apply {
            setOnShowListener {
                val layout =
                    (it as BottomSheetDialog).findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
                BottomSheetBehavior.from(layout).apply {
                    state = BottomSheetBehavior.STATE_COLLAPSED
                    isHideable = true
                    isCancelable = true
                    peekHeight = layout!!.height
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ThemeHolder
            .listen()
            .subscribe {
                if (it == Theme.NIGHT) backgroundLayout.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBackground1))
                else backgroundLayout.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBackground))
            }
            .connect()
    }

    override fun onStart() {
        super.onStart()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog?.window?.apply {
                findViewById<View>(com.google.android.material.R.id.container).fitsSystemWindows = false
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }

    protected fun Disposable.connect() = disposables.add(this)
}