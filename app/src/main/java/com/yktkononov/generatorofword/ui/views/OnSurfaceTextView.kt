package com.yktkononov.generatorofword.ui.views

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.yktkononov.generatorofword.Theme
import com.yktkononov.generatorofword.model.ThemeHolder
import io.reactivex.disposables.Disposable

class OnSurfaceTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : TextView(context, attrs, defStyleAttr) {

    private var disposable: Disposable? = null

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        disposable = ThemeHolder
            .listen()
            .subscribe { setTheme(it) }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        disposable?.dispose()
    }

    private fun setTheme(theme: Theme) {
        setTextColor(ContextCompat.getColor(context, theme.onSurfaceTextViewTheme.textColorResId))
    }
}