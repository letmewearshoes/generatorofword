package com.yktkononov.generatorofword

import com.yktkononov.generatorofword.entity.themes.ButtonTextViewTheme
import com.yktkononov.generatorofword.entity.themes.CardViewTheme
import com.yktkononov.generatorofword.entity.themes.OnSurfaceTextViewTheme
import com.yktkononov.generatorofword.entity.themes.StandardTextViewTheme

enum class Theme(
    val standardTextViewTheme: StandardTextViewTheme,
    val onSurfaceTextViewTheme: OnSurfaceTextViewTheme,
    val buttonTextViewTheme: ButtonTextViewTheme,
    val cardviewTheme: CardViewTheme
) {

    DAY(
        StandardTextViewTheme(R.color.colorOnBackground),
        OnSurfaceTextViewTheme(R.color.colorOnSurface),
        ButtonTextViewTheme(R.color.colorOnPrimary, R.drawable.bg_button_generate),
        CardViewTheme(R.color.colorSurface)
    ),
    NIGHT(
        StandardTextViewTheme(R.color.colorOnBackground1),
        OnSurfaceTextViewTheme(R.color.colorOnSurface1),
        ButtonTextViewTheme(R.color.colorOnPrimary1, R.drawable.bg_button_generate_night),
        CardViewTheme(R.color.colorSurface1)
    )
}