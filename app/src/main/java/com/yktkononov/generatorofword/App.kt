package com.yktkononov.generatorofword

import android.app.Application
import com.yktkononov.generatorofword.model.LanguageHolder
import com.yktkononov.generatorofword.model.ThemeHolder
import com.yktkononov.generatorofword.model.WordsHolder

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        LanguageHolder.init(defaultLanguage = Language.ENGLISH)
        WordsHolder.init(this, LanguageHolder.get())
        ThemeHolder.init(this)
    }
}